angular.module('iWater.controllers', [])

.controller('AuthCtrl', function($scope, $ionicConfig) {

})

// APP
.controller('AppCtrl', function($scope, $http,$rootScope,$ionicConfig,$localStorage) {

$scope.fullname = $localStorage.fullname;
/*
$http.get($rootScope.host+'/get_Tasks.php').success(function(response) {
		$scope.tasks = $localStorage;
		$localStorage.tasks = response;		
});
*/	

$http.get('js/json/data1.json').success(function(response) {
		$scope.tasks = $localStorage;
		$localStorage.tasks = response;		
		//console.log($localStorage.tasks[0].mission[0].add[0].remarks)
});

})


//LOGIN
.controller('LoginCtrl', function($scope,$http, $state, $templateCache, $q, $rootScope,$localStorage) {

if ($localStorage.usernameid) {
	window.location.href = "#/app/main";
}

$scope.LoginBtn = function() { 

//console.log ($rootScope.host);

	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';


			data = 
			{
				'email' : $scope.username,
				'password' : $scope.password
			};

			$http.post($rootScope.host+'/check_login.php', data)
			.success(function(data, status, headers, config)
			{
			//alert (data);
							userid = '32';
							//userid = data.response.userid;
							fullname = data.response.name;
							$scope.usernameid = $localStorage;
							$localStorage.usernameid = userid;	
							$scope.fullname = $localStorage;
							$localStorage.fullname = fullname;
							$state.go('app.main');

			})
			.error(function(data, status, headers, config)
			{
			});
		
		

}

/*
		initDatabase();

		$scope.LoginBtn = function() { 

		username = $scope.username;
		password = $scope.password;


		db.transaction(function (tx) {
		tx.executeSql('SELECT * FROM users WHERE username = ? AND password = ?', [username,password],function (tx, results) {
					 len = results.rows.length;
					 if (len =="0") {
						db.transaction(function (tx) { 
						tx.executeSql('INSERT INTO users (username, password) VALUES (?, ?)', [username,password],function (tx, results) {
					 	$scope.usernameid = $localStorage;
						$localStorage.usernameid = results.insertId;	
						$state.go('app.main');

					});	 
					});	 
					 }
					 else {
						obj = results.rows.item(0);
						$scope.usernameid = $localStorage;
						$scope.fullname = $localStorage;
						$localStorage.usernameid = obj.id;	
						$localStorage.fullname = obj.name;	
						$state.go('app.main');
					 }
					 
				  });
				  });




}
/*
	$scope.doLogIn = function(){
		$state.go('app.feeds-categories');
	};

	$scope.user = {};

	$scope.user.email = "john@doe.com";
	$scope.user.pin = "12345";

	// We need this for the form validation
	$scope.selected_tab = "";

	$scope.$on('my-tabs-changed', function (event, data) {
		$scope.selected_tab = data.title;
	});
*/
})

.controller('ProfileCtrl', function($scope, $state,$localStorage) {

initDatabase();

$scope.name = $localStorage.fullname;

$scope.changepwdBtn = function () {

name = $scope.name;

oldpassword = $scope.opassword;
newpassword = $scope.npassword;

			if (name) { 
					db.transaction(function (tx) { 
					tx.executeSql('UPDATE users SET name=? WHERE id=?', [name,$localStorage.usernameid], alert ("updated successfully"), onError);
					});
					$localStorage.fullname = name;	
			}

if (oldpassword && newpassword) {
	
				db.transaction(function (tx) {
				tx.executeSql('SELECT * FROM users WHERE id = ?', [$localStorage.usernameid],function (tx, results) {
					obj = results.rows.item(0);
					if (obj.password == oldpassword) {						
				    db.transaction(function (tx) { 
					tx.executeSql('UPDATE users SET name=? , password=? WHERE id=?', [name,newpassword,$localStorage.usernameid], alert ("updated successfully"), onError);
					});
					
					}
					else {
						alert ("wrong password");
					}
					});	 
					});	
	

}

}

})

.controller('SignupCtrl', function($scope, $state) {
	$scope.user = {};

	$scope.user.email = "john@doe.com";

	$scope.doSignUp = function(){
		$state.go('app.feeds-categories');
	};
})

.controller('TaskDetailsCtrl', function($http,$scope, $state,$stateParams) {

$http.get('js/json/data1.json').success(function(response) {
		$scope.watches = response[$stateParams.missionid].mission[$stateParams.id].add;
		//console.log($scope.watches)
	});

})


.controller('ForgotPasswordCtrl', function($scope, $state) {
	$scope.recoverPassword = function(){
		$state.go('app.feeds-categories');
	};

	$scope.user = {};
})

.controller('RateApp', function($scope) {
	$scope.rateApp = function(){
		if(ionic.Platform.isIOS()){
			//you need to set your own ios app id
			AppRate.preferences.storeAppURL.ios = '1234555553>';
			AppRate.promptForRating(true);
		}else if(ionic.Platform.isAndroid()){
			//you need to set your own android app id
			AppRate.preferences.storeAppURL.android = 'market://details?id=ionFB';
			AppRate.promptForRating(true);
		}
	};
})

.controller('SendMailCtrl', function($scope) {
	$scope.sendMail = function(){
		cordova.plugins.email.isAvailable(
			function (isAvailable) {
				// alert('Service is not available') unless isAvailable;
				cordova.plugins.email.open({
					to:      'envato@startapplabs.com',
					cc:      'hello@startapplabs.com',
					// bcc:     ['john@doe.com', 'jane@doe.com'],
					subject: 'Greetings',
					body:    'How are you? Nice greetings from IonFullApp'
				});
			}
		);
	};
})

.controller('MapsCtrl', function($scope, $ionicLoading) {

	$scope.info_position = {
		lat: 43.07493,
		lng: -89.381388
	};

	$scope.center_position = {
		lat: 43.07493,
		lng: -89.381388
	};

	$scope.my_location = "";

	$scope.$on('mapInitialized', function(event, map) {
		$scope.map = map;
	});

	$scope.centerOnMe= function(){

		$scope.positions = [];

		$ionicLoading.show({
			template: 'Loading...'
		});

		// with this function you can get the user’s current position
		// we use this plugin: https://github.com/apache/cordova-plugin-geolocation/
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			$scope.current_position = {lat: pos.G,lng: pos.K};
			$scope.my_location = pos.G+", "+pos.K;
			$scope.map.setCenter(pos);
			$ionicLoading.hide();
		});
	};
})

.controller('AdsCtrl', function($scope, $ionicActionSheet, AdMob, iAd) {

	$scope.manageAdMob = function() {

		// Show the action sheet
		var hideSheet = $ionicActionSheet.show({
			//Here you can add some more buttons
			buttons: [
				{ text: 'Show Banner' },
				{ text: 'Show Interstitial' }
			],
			destructiveText: 'Remove Ads',
			titleText: 'Choose the ad to show',
			cancelText: 'Cancel',
			cancel: function() {
				// add cancel code..
			},
			destructiveButtonClicked: function() {
				console.log("removing ads");
				AdMob.removeAds();
				return true;
			},
			buttonClicked: function(index, button) {
				if(button.text == 'Show Banner')
				{
					console.log("show banner");
					AdMob.showBanner();
				}

				if(button.text == 'Show Interstitial')
				{
					console.log("show interstitial");
					AdMob.showInterstitial();
				}

				return true;
			}
		});
	};

	$scope.manageiAd = function() {

		// Show the action sheet
		var hideSheet = $ionicActionSheet.show({
			//Here you can add some more buttons
			buttons: [
			{ text: 'Show iAd Banner' },
			{ text: 'Show iAd Interstitial' }
			],
			destructiveText: 'Remove Ads',
			titleText: 'Choose the ad to show - Interstitial only works in iPad',
			cancelText: 'Cancel',
			cancel: function() {
				// add cancel code..
			},
			destructiveButtonClicked: function() {
				console.log("removing ads");
				iAd.removeAds();
				return true;
			},
			buttonClicked: function(index, button) {
				if(button.text == 'Show iAd Banner')
				{
					console.log("show iAd banner");
					iAd.showBanner();
				}
				if(button.text == 'Show iAd Interstitial')
				{
					console.log("show iAd interstitial");
					iAd.showInterstitial();
				}
				return true;
			}
		});
	};
})

.controller('MainCtrl', function($scope, $state,$rootScope,$http,$localStorage) {

$scope.allTasksBtn = function()  {
$state.go('app.tasks');
}

$scope.notificationsBtn = function()  {
$state.go('app.push');
}


	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';


			data = 
			{
				'user' : $localStorage.usernameid
			//	'limit' : "5"

			};

			$http.post($rootScope.host+'/get_tasks.php', data)
			.success(function(data, status, headers, config)
			{
				$scope.alertnumber = data.response.length;
				$scope.quantity = 5;
				$scope.watches = data.response;
			})
			.error(function(data, status, headers, config)
			{

			});
		
	

/*
		initDatabase();
		
		$scope.json=[]; 
		
		db.transaction(function (tx) {
		tx.executeSql('SELECT * FROM user_missions WHERE user_index = ?', [$localStorage.usernameid],function (tx, results) {
		len = results.rows.length;
		  for (var i = 0; i < len; i++){

                                   $scope.json.push({   
                                        id: results.rows.item(i).id,
                                        title: results.rows.item(i).title,
                                        time: results.rows.item(i).time,
                                        location: results.rows.item(i).location,
                                        date: results.rows.item(i).date
                                    })

									
        }

							$scope.watches = $scope.json;				
							//console.log ($scope.array);
		});	



});
	

/*
	$http.get($scope.watches).success(function(response) {
		$scope.watches = response;
	});

*/

})


.filter("getCurrentDate", function(){
   return function(date){
      DateArray =  date.split("-");
	  StartDate = DateArray[2]+"/"+DateArray[1] ;
	  return StartDate;
      return output; 
   }
})

.filter("getCurrentColor", function(){
   return function(date)
   {
	  var Carray = new Array("#4fa212","#12a28c","#d3a7e3","#e9ac19","#50505c","#babac6");
	  var Nm = Math.floor(Math.random()*6)
      return Carray[Nm]; 
   }
})




.controller('TasksCtrl', function($scope, $http,$rootScope,$stateParams,$localStorage) {

	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';


			data = 
			{
				'user' : $localStorage.usernameid
			};

			$http.post($rootScope.host+'/get_tasks.php', data)
			.success(function(data, status, headers, config)
			{
				$scope.watches = data.response;
									
			})
			.error(function(data, status, headers, config)
			{

			});
			

	/*
	$scope.missionid = $stateParams.id;
	$http.get('js/json/data1.json').success(function(response) {
		//$scope.watches = response[$stateParams.id].mission[0].add;
		$scope.watches = response[$stateParams.id].mission;
		//console.log(response[$stateParams.id].mission.id);
	});	
	*/
})

.controller('CallsCtrl', function($scope, $http,$rootScope,$stateParams,$localStorage,$state,$ionicModal) {
$scope.$on('$ionicView.enter', function(e) {
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';


			taskid_data = 
			{
				'user' : $localStorage.usernameid,
				'taskid' : $stateParams.id
			};

			$http.post($rootScope.host+'/get_calls.php', taskid_data)
			.success(function(data, status, headers, config)
			{
				$scope.calls = data.response;
				$scope.json1 = data.response;
				console.log($scope.calls)
			})
			.error(function(data, status, headers, config)
			{

			});


			$http.post($rootScope.host+'/get_new_calls.php', taskid_data)
			.success(function(data, status, headers, config)
			{
				$scope.newcalls = data.response;
				$scope.json2 = data.response;
				console.log($scope.calls)
			})
			.error(function(data, status, headers, config)
			{

			});		
});

	$scope.AddNewPage = function() 
	{
		window.location.href = "#/app/newclock/"+$stateParams.id;
	}
	$scope.showNewArray = function() 
	{
		$scope.calls  = $scope.newcalls;
	}
	
	$scope.ReversArray = function()
	{
		$scope.calls  = $scope.json1;
		$scope.calls.reverse();	
		$scope.sortArray('0')	
	}
	
	
	
	$scope.sortArray = function(id) 
	{ 
		$scope.searchArray = function() 
		{
			return function(row) 
			{
				if (id == 0) 
				{
					return row;	
				}
				else if (id == 1) 
				{
					if($scope.calls  != $scope.json1)
					$scope.calls  = $scope.json1;
					return row.clock_number == 0;	
				}
				
				else if (id =="2") 
				{
					if($scope.calls  != $scope.newcalls)
					$scope.calls  = $scope.newcalls;
					return row.clock_number != -1;	
				}
				
				else if (id =="3") 
				{
					if($scope.calls  != $scope.json1)
					$scope.calls  = $scope.json1;
					return row.status == 1;
				}
				else if (id == 4) 
				{
					if($scope.calls  != $scope.json1)
					{
						$scope.calls  = $scope.json1;
						console.log("Array1")
					}
					return row.clock_number != null;	
				}
			}
		}
	}
		
		
		$scope.statsModal = function() 
		{
			//alert ($scope.clockdata)
			$ionicModal.fromTemplateUrl('views/app/layouts/stats.html', {
			  scope: $scope,
			  animation: 'slide-in-up'
			}).then(function(statsModalShow) {
			  $scope.statsModalShow = statsModalShow;
			   $scope.statsModalShow.show();
			});
		}
		$scope.statsModalClose = function() {
			$scope.statsModalShow.hide();
		}
			
		
	



})


// FEED
//brings all feed categories
.controller('FeedsCategoriesCtrl', function($scope, $http) {
	$scope.$on('$ionicView.enter', function(e) {
	$scope.feeds_categories = [];

	$http.get('feeds-categories.json').success(function(response) {
		$scope.feeds_categories = response;
	});
	});
	
})


.controller('ClockCtrl', function($scope, $http,$rootScope,$stateParams,$ionicConfig,$localStorage,$ionicModal,$ionicSlideBoxDelegate) {

$scope.$on('$ionicView.enter', function(e) {
	$scope.statuscode = 1;
//	$scope.currentcall = "";
	$scope.screenHeight = screen.height-100;
	$scope.pictureTaken = 0;
	
	$scope.fields = {
		"currentcall" : ""
	}
	
	$scope.onFileSelect = function (file) 
	{
		var fd = new FormData();
        fd.append('fileUPP', file.files[0]);
		fd.append('id', $stateParams.id);

		//console.log(fd)
		//console.log(file.files[0])
			console.log("ss")	
		$http.post($rootScope.host+'/UploadImg.php', fd, { transformRequest: angular.identity,"headers" : { "Content-Type" : undefined }}).success(function(data) 
		{
			$scope.pictureTaken = 1;
			$scope.getImages();
		//	if (data.response.imagepath) {
				
				//$("#Gallery").append("<div style='display:inline; margin-right:5px; margin-left:5px;'><img src="+$rootScope.host+"/"+data.response.imagepath+" width='30' height='30'></div>");

				//alert (data.response.imagepath);
				
			//}
         	//alert("p: "+data)  
        }).error(function(data) 
		{
			//alert(data.message); 	
		});
	};
	
	$scope.ComboChange = function ()
	{
		$scope.statuscode = parseInt($scope.statuscode);
	}
	
	$scope.getImages = function ()
	{
	
			$http.get($rootScope.host+'/get_clock_images.php?id='+$stateParams.id)
			.success(function(data, status, headers, config)
			{
				$scope.imageurl = $rootScope.host;
				$scope.watchesimages = data.response;
				console.log($scope.watchesimages)
			})
			.error(function(data, status, headers, config)
			{

			});	
	}
	
	$scope.getImages();
	
	$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';


			data = 
			{
				'id' : $stateParams.id
			};
			
			
			console.log("ss2")
			
			$http.post($rootScope.host+'/get_clock.php', data)
			.success(function(data, status, headers, config)
			{
				$scope.clockdata = data.response;
				$scope.clockid = data.response.id;
				$scope.statuscode = parseInt(data.response.status_code);
				$scope.customname = data.response.cst_name;
				$scope.prevcall = data.response.prev_call;
				$scope.curr_call = data.response.curr_call;
				$scope.consumption = 0;//data.response.consumption;
				$scope.address = data.response.address;
				$scope.remarks = data.response.remarks;
				console.log("ss4")
				console.log(data.response)
			/*
				if ($scope.curr_call) {
					
					$scope.prevcall = $scope.curr_call;
				}
				else {
					$scope.prevcall = $scope.prevcall;
				}
					
			*/

			})
			.error(function(data, status, headers, config)
			{

			});	
			console.log("ss : " + $scope.statuscode)

			
			$scope.inputChange = function()
			{
				if($scope.fields.currentcall > $scope.prevcall)
				{
					$scope.consumption = $scope.fields.currentcall-$scope.prevcall;
				}
				else
				{
					$scope.consumption = 0;
				}
			}
			
			$scope.focusOut = function()
			{
				
			}
			
			
			$scope.saveChanges = function () 
			{ 			
				if ($scope.statuscode != 1 && $scope.pictureTaken == 0) {
					alert ("יש לבצע צילום שעון");
				}
				else {
				
				if ($scope.fields.currentcall == undefined) 
				{
					alert ("יש להזין קריאה נוכחית");
				}
				else if ($scope.prevcall > $scope.fields.currentcall) 
				{
					
					alert ("ערך קריאה נוכחית קטן מערך קריאה קודמת יש לתקן");
				}
				else 
				{
					
					save_data = 
				{
					'id' : $stateParams.id,
					'currCall' : parseInt($scope.fields.currentcall),
					'clock_status' : $scope.statuscode
				};
			
					$http.post($rootScope.host+'/update_clock.php', save_data)
					.success(function(data, status, headers, config)
					{
					alert ("שעון עודכן בהצלחה");
					$scope.prevcall = $scope.fields.currentcall;
					window.history.back();
					})
					.error(function(data, status, headers, config)
					{
		
					});	
				}
			}
			}

	$scope.quantClick = function(type)
	     {
			   if(type == 1 && $scope.statuscode<5)
			   $scope.statuscode++;
			   else if(type == 2 && $scope.statuscode>1)
			   $scope.statuscode--
	     }
		
		$scope.resetBtn = function() {
			$scope.fields.currentcall = '';
			$scope.statuscode = $scope.clockdata.status_code;
		}
		
		$scope.showMoreinfo = function() {
			//alert ($scope.clockdata)
			$ionicModal.fromTemplateUrl('views/app/layouts/moreinfo_modal.html', {
			  scope: $scope,
			  animation: 'slide-in-up'
			}).then(function(showInfoModal) {
			  $scope.showInfoModal = showInfoModal;
			   $scope.showInfoModal.show();
			});
		}
		$scope.showMoreinfoClose = function() {
			$scope.showInfoModal.hide();
		}
		
		$scope.cancelBtn = function() { 
		window.history.back();
		}
		 
	 /*Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });
   
    $scope.$on('modal.hide', function() {
    
    });
  
    $scope.$on('modal.removed', function() {
     $scope.modal.remove();
	  console.log('Modal is remove!');
    });
    $scope.$on('modal.shown', function() {
      console.log('Modal is shown!');
    });*/

	
	 $ionicModal.fromTemplateUrl('image-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });
	
	/* $scope.next = function() {
      $ionicSlideBoxDelegate.next();
    };
  
    $scope.previous = function() {
      $ionicSlideBoxDelegate.previous();
    };
	
	
	
    $scope.openModal = function() {
      $ionicSlideBoxDelegate.slide(index);
      $scope.modal.show();
    };
*/
  
	
	 $scope.slideChanged = function(index) 
	 {
      	$scope.slideIndex = index;
		//alert(index)
     };
	 
	 $scope.closeModal = function() {
      	$scope.modal.hide();
     };
	 
	$scope.deleteImage = function(id) {
		
					$scope.currentImage = $ionicSlideBoxDelegate.currentIndex();
					$scope.imageId  = $scope.watchesimages[$scope.currentImage].id;
					
					image_params = 
					{
						'id' : $scope.imageId
					};
			
		 			$http.post($rootScope.host+'/delete_image.php', image_params)
					.success(function(data, status, headers, config)
					{
					$scope.watchesimages.splice($scope.currentImage, 1);
					$ionicSlideBoxDelegate.update();
					$scope.closeModal();

					alert ("תמונה נמחקה בהצלחה");
					})
					.error(function(data, status, headers, config)
					{
		
					});	
					
					

	}
	
	
	$scope.goToSlide = function(index) 
	{
		//console.log($scope.imageurl + "/"+$scope.watchesimages[index].image)
		 $ionicSlideBoxDelegate.update();
		$scope.currentImage = $scope.watchesimages[index].image;
      	$scope.modal.show();
		 $ionicSlideBoxDelegate.slide(index);
		//$scope.slideChanged(index)
		
       
		//console.log($scope.watchesimages[index])
		//alert (index);
    }
	setTimeout(function(){$ionicSlideBoxDelegate.slidesCount(),0});
	

});
})



.controller('ImportCtrl', function($scope, $http,$rootScope,$stateParams,$ionicConfig,$localStorage) {


})




.controller('NewClockCtrl', function($scope, $http,$rootScope,$stateParams,$ionicConfig,$localStorage) {
	
		$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

		$scope.statuscode = 1;
		$scope.submitbutton = false;
		
	$scope.onFileSelect = function (file) 
	{
		var fd = new FormData();
        fd.append('fileUPP', file.files[0]);
		$scope.submitbutton = true;
		
		//console.log(fd)
		//console.log(file.files[0])
		//	console.log("ss")	
		$http.post($rootScope.host+'/UploadImg.php', fd, { transformRequest: angular.identity,"headers" : { "Content-Type" : undefined }}).success(function(data) 
		{
			if (data.response.imagepath) 
			{
				$scope.image = data.response.imagepath;
			}
			$scope.submitbutton = false;
			
        }).error(function(data) 
		{
			//alert(data.message); 	
		});
	};

	
		$scope.quantClick = function(type)
	     {
			   if(type == 1 && $scope.statuscode<5)
			   $scope.statuscode++;
			   else if(type == 2 && $scope.statuscode>1)
			   $scope.statuscode--
	     }
		 
		$scope.ComboChange = function ()
		{
			$scope.statuscode = parseInt($scope.statuscode);
		}
	
	
		$scope.resetBtn = function() {
			$scope.fields.currentcall = '';
			$scope.statuscode = $scope.clockdata.status_code;
		}
		
		
		$scope.saveChanges = function()
		{
			//alert ($scope.statuscode);
			save_clock = 
			{
				'clock_id' : $stateParams.id,
				'clocknumber' : $scope.clocknumber,
				'custumer' : $scope.custumername,
				'currentcall' : $scope.currentcall,
				'prevcall' : $scope.prevcall,
				'statuscode' :  $scope.statuscode,
				'consumption' : $scope.consumption,
				'address' : $scope.address,
				'remarks' : $scope.remarks,
				'image' : $scope.image

			};
			//console.log(save_clock);
			$http.post($rootScope.host+'/add_new_clock.php', save_clock)
			.success(function(data, status, headers, config)
			{
				window.history.back();
			})
			.error(function(data, status, headers, config)
			{
			});
			
		}
		
})

.controller('ExportCtrl', function($scope, $http,$rootScope,$stateParams,$ionicConfig,$localStorage) {


})



.controller('NotificationsCtrl', function($scope, $http,$rootScope,$stateParams,$ionicConfig,$localStorage) {
$scope.newmessages = 2;

$http.get($rootScope.host+'/get_notifications.php').success(function(data) {
		$scope.notifications = data.response;		
});


$scope.refresh = function() { 
 location.reload(); 
}

})

.controller('ReadNotificationCtrl', function($scope, $http,$rootScope,$stateParams,$ionicConfig,$localStorage) {



$http.get($rootScope.host+'/get_notifications.php?id='+$stateParams.id).success(function(data) {
	$scope.title  = data.response.title;
	$scope.text = data.response.text;
});


})




.controller('PushCtrl', function($scope, $http,$rootScope,$stateParams,$ionicConfig,$localStorage) {

	   	$myarray = [];


    var pusher = new Pusher('cf338c95cfedb1acf0cf', {
      encrypted: false
    });
    var channel = pusher.subscribe('iWater');
    channel.bind('my_event', function(data) {
      //alert(data.message);
    });
	
	/*
	 	$scope.notifications = [
    {
        "title": 'test',
        //"date": "9:00"
     
    }
	]
*/	
	

	 // 	$scope.notifications.push($scope.data);

	//$scope.notifications = JSON.stringify(myarray);
	//alert ($scope.notifications)
})



//bring specific category providers
.controller('CategoryFeedsCtrl', function($scope, $http, $stateParams) {
	$scope.category_sources = [];

	$scope.categoryId = $stateParams.categoryId;

	$http.get('feeds-categories.json').success(function(response) {
		var category = _.find(response, {id: $scope.categoryId});
		$scope.categoryTitle = category.title;
		$scope.category_sources = category.feed_sources;
	});
})

//this method brings posts for a source provider
.controller('FeedEntriesCtrl', function($scope, $stateParams, $http, FeedList, $q, $ionicLoading, BookMarkService) {
	$scope.feed = [];

	var categoryId = $stateParams.categoryId,
			sourceId = $stateParams.sourceId;

	$scope.doRefresh = function() {

		$http.get('feeds-categories.json').success(function(response) {

			$ionicLoading.show({
				template: 'Loading entries...'
			});

			var category = _.find(response, {id: categoryId }),
					source = _.find(category.feed_sources, {id: sourceId });

			$scope.sourceTitle = source.title;

			FeedList.get(source.url)
			.then(function (result) {
				$scope.feed = result.feed;
				$ionicLoading.hide();
				$scope.$broadcast('scroll.refreshComplete');
			}, function (reason) {
				$ionicLoading.hide();
				$scope.$broadcast('scroll.refreshComplete');
			});
		});
	};

	$scope.doRefresh();

	$scope.bookmarkPost = function(post){
		$ionicLoading.show({ template: 'Post Saved!', noBackdrop: true, duration: 1000 });
		BookMarkService.bookmarkFeedPost(post);
	};
})

// SETTINGS
.controller('SettingsCtrl', function($scope, $ionicActionSheet, $state,$localStorage) {

	// Triggered on a the logOut button click
	$scope.showLogOutMenu = function() {

		// Show the action sheet
		var hideSheet = $ionicActionSheet.show({

			destructiveText: 'התנתקות',
			titleText: 'האם לאשר התנתקות?',
			cancelText: 'ביטול',
			cancel: function() {
				// add cancel code..
			},
			buttonClicked: function(index) {
				//Called when one of the non-destructive buttons is clicked,
				//with the index of the button that was clicked and the button object.
				//Return true to close the action sheet, or false to keep it opened.
				return true;
			},
			destructiveButtonClicked: function(){
				$localStorage.usernameid = '';
				$state.go('app.login');
			}
		});

	};
})

// TINDER CARDS
.controller('TinderCardsCtrl', function($scope, $http) {

	$scope.cards = [];


	$scope.addCard = function(img, name) {
		var newCard = {image: img, name: name};
		newCard.id = Math.random();
		$scope.cards.unshift(angular.extend({}, newCard));
	};

	$scope.addCards = function(count) {
		$http.get('http://api.randomuser.me/?results=' + count).then(function(value) {
			angular.forEach(value.data.results, function (v) {
				$scope.addCard(v.user.picture.large, v.user.name.first + " " + v.user.name.last);
			});
		});
	};

	$scope.addFirstCards = function() {
		$scope.addCard("https://dl.dropboxusercontent.com/u/30675090/envato/tinder-cards/left.png","Nope");
		$scope.addCard("https://dl.dropboxusercontent.com/u/30675090/envato/tinder-cards/right.png", "Yes");
	};

	$scope.addFirstCards();
	$scope.addCards(5);

	$scope.cardDestroyed = function(index) {
		$scope.cards.splice(index, 1);
		$scope.addCards(1);
	};

	$scope.transitionOut = function(card) {
		console.log('card transition out');
	};

	$scope.transitionRight = function(card) {
		console.log('card removed to the right');
		console.log(card);
	};

	$scope.transitionLeft = function(card) {
		console.log('card removed to the left');
		console.log(card);
	};
})


// BOOKMARKS
.controller('BookMarksCtrl', function($scope, $rootScope, BookMarkService, $state) {

	$scope.bookmarks = BookMarkService.getBookmarks();

	// When a new post is bookmarked, we should update bookmarks list
	$rootScope.$on("new-bookmark", function(event){
		$scope.bookmarks = BookMarkService.getBookmarks();
	});

	$scope.goToFeedPost = function(link){
		window.open(link, '_blank', 'location=yes');
	};
	$scope.goToWordpressPost = function(postId){
		$state.go('app.post', {postId: postId});
	};
})

// WORDPRESS
.controller('WordpressCtrl', function($scope, $http, $ionicLoading, PostService, BookMarkService) {
	$scope.posts = [];
	$scope.page = 1;
	$scope.totalPages = 1;

	$scope.doRefresh = function() {
		$ionicLoading.show({
			template: 'Loading posts...'
		});

		//Always bring me the latest posts => page=1
		PostService.getRecentPosts(1)
		.then(function(data){
			$scope.totalPages = data.pages;
			$scope.posts = PostService.shortenPosts(data.posts);

			$ionicLoading.hide();
			$scope.$broadcast('scroll.refreshComplete');
		});
	};

	$scope.loadMoreData = function(){
		$scope.page += 1;

		PostService.getRecentPosts($scope.page)
		.then(function(data){
			//We will update this value in every request because new posts can be created
			$scope.totalPages = data.pages;
			var new_posts = PostService.shortenPosts(data.posts);
			$scope.posts = $scope.posts.concat(new_posts);

			$scope.$broadcast('scroll.infiniteScrollComplete');
		});
	};

	$scope.moreDataCanBeLoaded = function(){
		return $scope.totalPages > $scope.page;
	};

	$scope.bookmarkPost = function(post){
		$ionicLoading.show({ template: 'Post Saved!', noBackdrop: true, duration: 1000 });
		BookMarkService.bookmarkWordpressPost(post);
	};

	$scope.doRefresh();
})

// WORDPRESS POST
.controller('WordpressPostCtrl', function($scope, post_data, $ionicLoading) {

	$scope.post = post_data.post;
	$ionicLoading.hide();

	$scope.sharePost = function(link){
		window.plugins.socialsharing.share('Check this post here: ', null, null, link);
	};
})




.controller('ImagePickerCtrl', function($scope, $rootScope, $cordovaCamera) {

	$scope.images = [];

	$scope.selImages = function() {

		window.imagePicker.getPictures(
			function(results) {
				for (var i = 0; i < results.length; i++) {
					console.log('Image URI: ' + results[i]);
					$scope.images.push(results[i]);
				}
				if(!$scope.$$phase) {
					$scope.$apply();
				}
			}, function (error) {
				console.log('Error: ' + error);
			}
		);
	};

	$scope.removeImage = function(image) {
		$scope.images = _.without($scope.images, image);
	};

	$scope.shareImage = function(image) {
		window.plugins.socialsharing.share(null, null, image);
	};

	$scope.shareAll = function() {
		window.plugins.socialsharing.share(null, null, $scope.images);
	};
})


.controller('DetailsCtrl', function($scope, $rootScope, $cordovaCamera) {

	$scope.images = [];

	$scope.selImages = function() {

		window.imagePicker.getPictures(
			function(results) {
				for (var i = 0; i < results.length; i++) {
					console.log('Image URI: ' + results[i]);
					$scope.images.push(results[i]);
				}
				if(!$scope.$$phase) {
					$scope.$apply();
				}
			}, function (error) {
				console.log('Error: ' + error);
			}
		);
	};

	$scope.removeImage = function(image) {
		$scope.images = _.without($scope.images, image);
	};

	$scope.shareImage = function(image) {
		window.plugins.socialsharing.share(null, null, image);
	};

	$scope.shareAll = function() {
		window.plugins.socialsharing.share(null, null, $scope.images);
	};
})




