	 createUsersStatement = 'CREATE  TABLE  IF NOT EXISTS "users" ("id" INTEGER PRIMARY KEY AUTOINCREMENT, "name" VARCHAR, "username" VARCHAR, "password" VARCHAR);';
	 createMissionsStatement = 'CREATE  TABLE  IF NOT EXISTS "user_missions" ("id" INTEGER PRIMARY KEY AUTOINCREMENT,"user_index" INTEGER,   "title" VARCHAR,  "time" DATETIME, "location" VARCHAR, "date" DATETIME);';
	 
		db = openDatabase("iWater", "1.0", "iWater App", 200000);

	function initDatabase() { 
	try {
	if (!window.openDatabase){ 
		console.log('Databases are not supported in this browser.');
		}
		else {
		createTable(); 
		}
		}
	   catch (e) {
	   if (e == 2) {
	  // Version number mismatch. 
	   console.log("Invalid database version.");
	   } 
	   else {
			console.log("Unknown error " + e + ".");
			}
			return;
		}
	}

		function createTable()  { 
		db.transaction(function (tx) { 
		tx.executeSql(createUsersStatement, [], sqliteSuccess, onError);
		 });
		 	 
		 db.transaction(function (tx) { 
		tx.executeSql(createMissionsStatement, [], sqliteSuccess, onError);
		 });
		
		/*
		 db.transaction(function (tx) { 
		tx.executeSql('INSERT INTO user_missions (user_index,title,time,location,date) VALUES ("1","title test","9:00","Haifa","20-08-2015")', [], sqliteSuccess, onError);
		 });
	*/
		 
		 
		 
		}

		function onError(tx, error) { 
			console.log(error.message);
		}

		function sqliteSuccess() { 
		} 